### Resumen ###

(Resuma brevemente el bug encontrado)


### Pasos para reproducirlo ###

(Cómo debería procederse para reproducir el problema - esto es muy importante)


### Ejemplo de reporte de bug ###

¿Cuál es el comportamiento del bug actual?
(Lo que sucede realmente)

¿Cuál sería el comportamiento correcto esperado?
(Lo que desearía que sucediera)

Archivos de logs relevantes y/o capturas de pantalla

(Adjunte y/o pegue todos los archivos de logs relevantes -por favor use bloques 
de código (```) para dar formato a la salida de consola. De otra forma, se hace
muy difícil la lectura del código y de los logs.)

### Posibles errores

(Si puede, enlace las líneas de código que pueden ser responsables del
problema

/label ~bug ~reproduced ~needs-investigation
/cc @project-manager
/assign @qa-tester