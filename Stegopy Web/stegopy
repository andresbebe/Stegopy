#!/usr/bin/env python3
#  -*- coding: UTF-8 -*-

# Stegopy - Stegopy es un módulo básico que implementa esteganografía
# Copyright (C) 2018 alb <tatuss@ciudad.com.ar> tux1t0 <tux1t0@disroot.org>
#
# For more information: https://gitlab.com/andresbebe/Stegopy
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY, without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>


"""
     _
 ___| |_ ___  __ _  ___  _ __  _   _
/ __| __/ _ \/ _' |/ _ \| '_ \| | | |
\__ \ ||  __/ ( | | (_) | |_) | |_| |
|___/\__\___|\__, |\___/| .__/ \__, |
             |___/      |_|    |___/

Usage:
stegopy dct hide <input> <output>
stegopy dct unhide <input>
stegopy lsb hide <file> <clave_pub> <input> <output>
stegopy lsb unhide <clave_priv> <clave_AES> <input> <output>
stegopy test
stegopy -h (--help)
stegopy -v (--version)

Options:
    dct             Método esteganográfico de la transformada dicreta
                    del coseno (DCT)
    lsb             Método esteganográfico del último bit significativo
                    (lsb)
    hide            Ocultación del estegosecreto
    unhide          Revelado del estegosecreto
    <clave pub>     Clave pública RSA (formato PEM)
    <clave priv>    Clave privada RSA (formato PEM)
    <clave AES>     Clave AES cifrada con RSA
    <file>          Archivo de texto a ocultar
    <input>         Archivo de imagen portadora (hide) o archivo con el
                    mensaje oculto (unhide)
    <output>        Imagen con mensaje oculto (hide) o mensaje descifrado
                    (unhide)
    -h  --help      Muestra esta ayuda
    --version   Muestra la versión del programa

 dct hide: se oculta un mensaje ingresado por consola en la imagen <input>
        (de formato jpe/jpeg) y se graba en la imagen <output> (de formato jp2)

 dct unhide: se extrae el mensaje oculto en la imagen <input>(de formato jp2)
        y se muestra por la pantalla de la consola.

 lsb hide: el archivo de texto <file> se oculta en la imagen <input> (formato png)
        y se almacena en <output> (formato png) cifrando la clave AES generada por
        stegopy y almacenada en el archivo fernet.key con la clave pública RSA
        <clave_pub>

 lsb unhide: el mensaje oculto en <input> (formato png) se extrae en <output>
        (formato png) con la clave AES <clave_AES> y la clave RSA privada
        <clave_priv>

 test: se generan las claves RSA privada y pública y se almacenan como
       RSA_privada.pem y RSA_publica.pem respectivamente.

"""
import docopt
import bitarray
from modulos import dct
from modulos import lsb
from modulos import cripto
from modulos import test
from modulos import compresion

def main():
    '''Función principal'''
    args = docopt.docopt(__doc__, version="0.5.1")

    if args['test']:
    # generación y almacenamiento de una pareja de claves RSA
        private_key = cripto.generar_clave_rsa()
        public_key = private_key.public_key()
        cripto.almacenar_claves_rsa(private_key)
        print("Claves privada y pública RSA generadas y almacenadas")

    if args['hide']:
    # carga argumentos de imágenes
        portadora = args['<input>']
        estego_archivo = args['<output>']

        if args['dct']:
            test.imagen(portadora, 'dct')
            test.imagen(estego_archivo, 'dct_hide')
            dct.ocultar_mensaje(portadora, estego_archivo)
            print("El mensaje se ha ocultado.")

        elif args['lsb']:
            # carga argumentos y comprueba archivos
            archivo = args['<file>']
            public_key = args['<clave_pub>']
            test.texto(archivo)
            test.clave_rsa(public_key)
            test.imagen(portadora, 'lsb')
            test.imagen(estego_archivo, 'lsb_hide')

            # lee el archivo a ocultar y lo comprime
            binario_lzma = compresion.compr_lzma(archivo)

            # cifra el archivo y la clave AES
            clave_aes, mensaje_cifrado = cripto.cifrado_fernet(binario_lzma)
            clave_aes_cifrada = cripto.cifrado_rsa(public_key, clave_aes)
            cripto.almacenar_clave_aes(clave_aes_cifrada)

            # oculta al mensaje cifrado
            lsb.ocultar_mensaje(portadora, mensaje_cifrado, estego_archivo)
            print("El archivo '" + archivo + "' se ha ocultado")


    if args['unhide']:
        # carga argumentos y comprueba archivos
        estego_archivo = args['<input>']

        if args['dct']:
            test.imagen(estego_archivo, 'dct_unhide')
            salida = dct.extraer_mensaje(estego_archivo)
            mensaje = bitarray.bitarray(salida).tobytes().decode('UTF-8')
            print("Mensaje secreto:")
            print(mensaje)

        elif args['lsb']:
            # carga argumentos y comprueba archivos
            test.imagen(estego_archivo, 'lsb')
            private_key = args['<clave_priv>']
            clave = args['<clave_AES>']
            archivo = args['<output>']
            test.clave_rsa(private_key)
            test.clave_aes(clave)

            # descifra la clave AES
            clave_aes_cifrada = cripto.cargar_clave_simetrica(clave)
            clave_aes = cripto.descifrado_rsa(private_key, clave_aes_cifrada, clave)

            # extraccion del mensaje oculto y conversion a binario
            mensaje = lsb.extraer_mensaje(estego_archivo)

            # descifrado del mensaje
            mensaje_cifrado = mensaje.encode('UTF-8')
            mensaje_lzma = cripto.descifrado_fernet(
                clave_aes,
                mensaje_cifrado,
                estego_archivo
                )

            # descomprime el mensaje y lo almacena
            compresion.descompr_lzma(mensaje_lzma, archivo)

            print("El archivo descifrado se ha guardado en '" + archivo + "'")

if __name__ == "__main__":
    main()
