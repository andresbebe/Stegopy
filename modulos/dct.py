'''Módulo que implementa el método de DCT de Zhao-Koch.'''

import sys
import cv2
import numpy as np
import bitarray
from skimage.util import view_as_blocks
from scipy.fftpack import dct, idct
from PIL import Image

# coordenadas (u,v) de los coeficientes dentro del bloque de 8x8 pixels
U1, V1 = 4, 1
U2, V2 = 3, 2

# tamaño en bits (para un bloque nxn)
N = 8

# UMBRAL de comparación
UMBRAL = 30

def doble_a_byte(arr):
    '''Conversión de double a byte'''
    return np.uint8(np.round(np.clip(arr, 0, 255), 0))


def aumento_abs(valor):
    '''Calcula el incremento en valor absoluto'''
    return valor + 1 if valor >= 0 else valor - 1


def disminucion_abs(valor):
    '''Calcula el decremento en valor absoluto'''
    if np.abs(valor) <= 1:
        return 0
    return valor - 1 if valor >= 0 else valor + 1


def diferencia_abs_coefs(transformada):
    '''Retorna la diferencia absoluta entre coeficientes'''
    return abs(transformada[U1, V1]) - abs(transformada[U2, V2])


def validar_coeficientes(transformada, bit):
    '''Verifica los coeficientes'''
    diferencia = diferencia_abs_coefs(transformada)
    if (bit == 0) and (diferencia > UMBRAL):
        return True
    if (bit == 1) and (diferencia < -UMBRAL):
        return True
    return False


def cambiar_coeficientes(transformada, bit):
    '''Intercambio de coeficientes en la transformada'''
    coeficientes = transformada.copy()
    if bit == 0:
        coeficientes[U1, V1] = aumento_abs(coeficientes[U1, V1])
        coeficientes[U2, V2] = disminucion_abs(coeficientes[U2, V2])
    elif bit == 1:
        coeficientes[U1, V1] = disminucion_abs(coeficientes[U1, V1])
        coeficientes[U2, V2] = aumento_abs(coeficientes[U2, V2])
    return coeficientes


def insertar_bit(bloque, bit):
    '''Inserta un bit en la imagen'''
    parche = bloque.copy()
    coeficientes = dct(dct(parche, axis=0), axis=1)
    while not validar_coeficientes(coeficientes, bit) or (bit != recuperar_bit(parche)):
        coeficientes = cambiar_coeficientes(coeficientes, bit)
        parche = doble_a_byte(idct(idct(coeficientes, axis=0), axis=1)/(2*N)**2)
    return parche


def ocultar_mensaje(imagen_portadora, estego_archivo):
    '''Oculta el archivo secreto en una imagen'''

    # Abre el archivo con la imagen original para lectura
    archivo_imagen = Image.open(imagen_portadora)
    imagen = np.array(archivo_imagen)
    try:
        filas, columnas = imagen.shape[:2]
    except AttributeError:
        print("Error: no se encontró el archivo '" + imagen_portadora + "'")
        sys.exit()

    # redimensiona la imagen para subdividirla en bloques de 8x8
    if filas%N != 0 or columnas%N != 0:
        imagen = cv2.resize(imagen, (columnas + (N -columnas % N), filas + (N - filas % N)))
        filas, columnas = imagen.shape[:2]

    # Calcula el tamaño máximo del mensaje a ocultar
    maximo = int((filas * columnas) / (N * N * N) - 1)
    print("Ingrese el mensaje a ocultar,", end=" ")
    mensaje = input("máximo " + str(maximo) + " caracteres: ")
    if len(mensaje) > maximo:
        print("El mensaje es demasiado extenso, posee " + str(len(mensaje)) + " caracteres.")
        sys.exit()

    # Transforma el mensaje secreto en una lista de bits
    bit_array = bitarray.bitarray()
    lista_binaria = []
    mensaje_binario = []
    bit_array.frombytes(mensaje.encode('UTF-8'))
    lista = bit_array.tolist()
    for elemento  in lista:
        lista_binaria.append(int(elemento))
    for elemento in lista_binaria:
        mensaje_binario.append(elemento)

    # agrega caracter de final de mensaje
    mensaje_binario.extend([0, 0, 0, 0, 0, 0, 0, 0])

    # Separa el canal azul y oculta los bits del mensaje en la imagen
    canal_azul = np.ascontiguousarray(imagen[:, :, 2])
    bloques = view_as_blocks(canal_azul, block_shape=(N, N))
    bloque_h = bloques.shape[1]
    for indice, bit in enumerate(mensaje_binario):
        i = indice // bloque_h
        j = indice % bloque_h
        bloque = bloques[i, j]
        canal_azul[i*N: (i+1)*N, j*N: (j+1)*N] = insertar_bit(bloque, bit)
    imagen[:, :, 2] = canal_azul
    imagen_salida = Image.fromarray(imagen, mode='RGB')
    imagen_salida.save(estego_archivo, format='JPEG2000', subsampling=-1, quality=100)
    print("La imagen esteganográfica se ha grabado en el archivo '"+ estego_archivo + "'.")


def sublistas(lista):
    '''Separa la lista binaria en sublistas de menor tamaño'''
    for i in range(0, len(lista), N):
        yield lista[i:i+N]


def recuperar_bit(bloque):
    '''Extrae los bits del mensaje'''
    transformada = dct(dct(bloque, axis=0), axis=1)
    return 0 if diferencia_abs_coefs(transformada) > 0 else 1


def extraer_mensaje(estego_archivo):
    '''Extrae el mensaje secreto de la estego imagen'''
    # Abre el archivo de la imagen
    archivo_imagen = Image.open(estego_archivo)
    imagen = np.array(archivo_imagen)
    try:
        canal_azul = np.ascontiguousarray(imagen[:, :, 2])
    except TypeError:
        print("Error: no se encuentra el archivo '" + estego_archivo + "'")
        sys.exit()
    filas, columnas = imagen.shape[:2]


    # Divide la imagen en bloques
    try:
        bloques = view_as_blocks(canal_azul, block_shape=(N, N))
    except ValueError:
        print("La imagen '" + estego_archivo + "' no es la correcta")
        sys.exit()
    h = bloques.shape[1]

    # Extrae los bits de los bloques y los almacena en una lista binaria
    indice = 0
    longitud = 0
    lista_binaria = []
    total_bloques = int(filas/N * columnas/N)
    lista_binaria = [recuperar_bit(bloques[indice//h, indice%h]) for indice in range(total_bloques)]

    # separa la lista binaria en sublistas de 8 bits para armar los caracteres
    sublista = list(sublistas(lista_binaria))

    # calcula la longitud del mensaje
    for elemento in sublista:
        valor = listas_iguales(elemento, [0, 0, 0, 0, 0, 0, 0, 0])
        if valor:
            break
        else:
            longitud += 1
    longitud = N * longitud

    # devuelve el mensaje
    try:
        return [lista_binaria[indice] for indice in range(longitud)]
    except:
        print("No se ha encontrado un mensaje oculto en '" + estego_archivo + "'")


def listas_iguales(lista_a, lista_b):
    ''' Compara si dos listas son iguales'''
    lista_a.sort()
    if len(lista_a) != len(lista_b):
        return False
    for i in range(0, len(lista_a)):
        if lista_a[i] != lista_b[i]:
            return False
    return True
