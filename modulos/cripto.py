'''Módulo que implementa cifrado AES y RSA.'''

import sys
from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives import hashes
from cryptography.hazmat.primitives.asymmetric import rsa
from cryptography.hazmat.primitives.asymmetric import padding
from cryptography.hazmat.primitives import serialization
from cryptography.fernet import Fernet

def generar_clave_rsa():
    '''Genera una pareja de claves RSA para probar el algoritmo.'''
    private_key = rsa.generate_private_key(
        public_exponent=65537,
        key_size=4096,
        backend=default_backend()
    )
    return private_key


def almacenar_claves_rsa(private_key):
    '''Almacena las claves RSA pública y privada en dos archivos.

       RSA_publica.pem y RSA_privada.pem.'''
    # Serializa la clave privada en formato PEM
    priv_pem = private_key.private_bytes(
        encoding=serialization.Encoding.PEM,
        format=serialization.PrivateFormat.TraditionalOpenSSL,
        encryption_algorithm=serialization.NoEncryption()
        )

    # Almacena la clave privada en disco
    with open('RSA_privada.pem', 'wb') as priv:
        priv.write(priv_pem)

    # serializa la clave pública en formato PEM
    public_key = private_key.public_key()
    public_pem = public_key.public_bytes(
        encoding=serialization.Encoding.PEM,
        format=serialization.PublicFormat.SubjectPublicKeyInfo
        )

    # Almacena la clave pública en disco
    with open('RSA_publica.pem', 'wb') as pub:
        pub.write(public_pem)


def cargar_clave_privada(clave):
    '''Lee desde un archivo la clave RSA privada.'''
    try:
        with open(clave, 'rb') as key_file:
            private_key = serialization.load_pem_private_key(
                key_file.read(),
                None,
                backend=default_backend()
            )
        return private_key
    except FileNotFoundError:
        print("Error de clave asimétrica:", end=" ")
        print("no se encuentra el archivo '" + clave + "'.")
        sys.exit()
    except ValueError:
        print("Error de clave asimétrica:", end=" ")
        print("'" + clave + "' no es una clave RSA privada")
        sys.exit()


def cargar_clave_publica(clave):
    '''Lee desde un archivo la clave RSA pública.'''
    try:
        with open(clave, 'rb') as key_file:
            public_key = serialization.load_pem_public_key(
                key_file.read(),
                backend=default_backend()
                )
        return public_key
    except ValueError:
        print("Error de clave:", end=" ")
        print("'" + clave + "' no es una clave RSA pública")
        sys.exit()
    except FileNotFoundError:
        print("Error de clave:", end=" ")
        print("no se encuentra el archivo '" + clave + "'.")
        sys.exit()


def cargar_clave_simetrica(clave):
    '''Lee desde un archivo una clave simetrica.'''
    try:
        with open(clave, 'rb') as archivo:
            key = archivo.read()
        return key
    except FileNotFoundError:
        print("Error de clave simétrica:", end=" ")
        print("no se encuentra el archivo '" + clave + "'.")
        sys.exit()


def cifrado_rsa(clave, clave_aes):
    '''Cifra la clave AES con la clave RSA pública.'''
    # carga la clave pública RSA
    public_key = cargar_clave_publica(clave)

    # cifra la clave AES
    clave_aes_cifrada = public_key.encrypt(
        clave_aes,
        padding.OAEP(
            mgf=padding.MGF1(algorithm=hashes.SHA512()),
            algorithm=hashes.SHA512(),
            label=None
        )
    )

    # devuelve la clave AES cifrada
    return clave_aes_cifrada


def almacenar_clave_aes(clave):
    '''Almacena la clave AES en el archivo fernet.key.'''
    with open('fernet.key', 'wb') as archivo:
        archivo.write(clave)


def cifrado_fernet(mensaje_binario):
    '''Cifra el mensaje con la clave AES.'''
    key = Fernet.generate_key()
    fernet = Fernet(key)
    mensaje = fernet.encrypt(mensaje_binario)
    return key, mensaje


def descifrado_fernet(clave_aes, mensaje_cifrado, imagen):
    '''Descifra el mensaje con la clave AES.'''
    fernet = Fernet(clave_aes)
    try:
        mensaje_descifrado = fernet.decrypt(mensaje_cifrado)
        return mensaje_descifrado
    except:
        print("Error en descifrado del archivo:", end=" ")
        print("¿Es '" + imagen + "' la estego imagen correcta?")
        sys.exit()


def descifrado_rsa(rsa_privada, clave_aes_cifrada, clave):
    '''Descifra la clave AES con la clave RSA privada.'''
    # carga la clave privada RSA
    private_key = cargar_clave_privada(rsa_privada)

    try:
        # descifra la clave AES
        clave_aes = private_key.decrypt(
            clave_aes_cifrada,
            padding.OAEP(
                mgf=padding.MGF1(algorithm=hashes.SHA512()),
                algorithm=hashes.SHA512(),
                label=None
            )
        )
        # devuelve la clave AES descifrada
        return clave_aes
    except ValueError:
        print("Error de descifrado de clave simétrica.", end=" ")
        print("Verifique lo siguiente:")
        print("1) Que '" + rsa_privada + "' sea la clave RSA privada correcta")
        print("2) Que '" + clave + "' sea la clave AES correcta")
        sys.exit()
