'''Modulo que implementa la técnica del LSB en esteganografía.'''

import sys
from PIL import Image

def setear_lsb(valor, bit):
    '''Enmascara el último bit significativo (LSB).'''
    if bit == '0':
        valor = valor & 254
    else:
        valor = valor | 1
    return valor


def ocultar_mensaje(imagen_portadora, mensaje, estego_archivo):
    '''Oculta el mensaje cifrado en la imagen portadora mediante LSB.'''
    # Añade 0x00 para indicar al fin de la cadena
    mensaje_cifrado = mensaje.decode('UTF-8')
    mensaje_cifrado += chr(0)

    # Crea las imágenes portadora y de ocultación
    try:
        portadora = Image.open(imagen_portadora)
        portadora = portadora.convert('RGBA')
        salida = Image.new(portadora.mode, portadora.size)
    except FileNotFoundError:
        print("Error: no se encontró el archivo '" + imagen_portadora + "'")
        sys.exit()

    # Crea un array para los pixels de la imagen portadora
    lista_pixels = list(portadora.getdata())

    # Crea un array para los pixels combinados de la imagen
    # transportadora y la imagen de ocultacion
    array_nuevo = []

    # Recorrida por todos los caracteres del mensaje
    for i, caracter in enumerate(mensaje_cifrado):

        # conversion de caracter a entero
        char_a_entero = ord(caracter)

        # conversion del entero a cadena de binarios de longitud 8
        cadena_binaria = str(bin(char_a_entero))[2:].zfill(8)
        # variables para almacenar dos pixels por cada caracter
        try:
            pixel1 = lista_pixels[i*2]
            pixel2 = lista_pixels[(i*2)+1]
        except IndexError:
            print("Error de tamaño:", end=" ")
            print("el archivo a ocultar es demasiado grande.")
            print("Busque una imagen de mayor tamaño.")
            sys.exit()

        # arrays para almacenar los valores de los datos ocultos
        pixel_nuevo1 = []
        pixel_nuevo2 = []

        # Iteracion de cuatro ciclos para modificar los valores
        # R,G,B y A de los pixels
        for j in range(0, 4):

            # almacena los primeros 4 bits del caracter
            pixel_nuevo1.append(setear_lsb(pixel1[j], cadena_binaria[j]))

            # almacena los ultimos 4 bits del caracter
            pixel_nuevo2.append(setear_lsb(pixel2[j], cadena_binaria[j+4]))

        # conversion a tuplas y agregado al nuevo array
        array_nuevo.append(tuple(pixel_nuevo1))
        array_nuevo.append(tuple(pixel_nuevo2))

    # extiende el array a los pixels restantes de la imagen
    # transportadora y guarda el estego archivo
    array_nuevo.extend(lista_pixels[len(mensaje_cifrado)*2:])
    salida.putdata(array_nuevo,
                   scale=1.0,
                   offset=0.0
                  )
    salida.save(estego_archivo)
    print("La imagen esteganográfica se ha grabado en '"+ estego_archivo + "'.")


def obtener_pares_pixels(iterable):
    '''Extrae los pixels de una lista y los retorna de a pares.

       Cada caracter del mensaje ocupa dos pixels.'''
    pixel = iter(iterable)
    return zip(pixel, pixel)


def obtener_lsb(valor):
    ''' Obtiene el LSB para un valor RGBA y lo retorna como string.'''
    if valor & 1 == 0:
        return '0'
    return '1'


def extraer_mensaje(estego_archivo):
    '''Extrae el mensaje oculto.'''
    # crea una imagen para el estego archivo
    try:
        imagen = Image.open(estego_archivo)
    except FileNotFoundError:
        print("Error de archivo:", end=" ")
        print("no se encuentra a '" + estego_archivo + "'.")
        sys.exit()

    # crea un array de pixels con los datos de la imagen
    lista_pixels = list(imagen.getdata())

    # crea un string vacio para el mensaje a extraer
    mensaje = ""

    # itera sobre los pares de pixels que devuelve la funcion
    for pixel1, pixel2 in obtener_pares_pixels(lista_pixels):

        # crea un string binario
        byte_mensaje = "0b"

        # itera sobre los valores RGBA en cada pixel, obtiene
        # el LSB y lo agrega al string binario
        for pix in pixel1:
            byte_mensaje += obtener_lsb(pix)

        for pix in pixel2:
            byte_mensaje += obtener_lsb(pix)

        # condicion de fin de string (caracter 0x00)
        if byte_mensaje == "0b00000000":
            break

        # convierte el byte a su caracter original y lo agrega
        # al mensaje
        mensaje += chr(int(byte_mensaje, 2))
    # devuelve el mensaje
    return mensaje
