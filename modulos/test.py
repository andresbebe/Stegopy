'''Módulo que valida extensiones de archivos.'''

import os
import sys
from PIL import Image

def imagen(archivo_imagen, metodo):
    '''Comprueba el formato del archivo de la imagen portadora.'''
    if metodo == 'lsb_hide':
        nombre, extension = os.path.splitext(archivo_imagen)
        if extension != '.png':
            print("Error de formato de archivo.", end=" ")
            print("El archivo '" + nombre + extension + "' debe tener extensión .png")
            sys.exit()
    elif metodo == 'dct_hide':
        nombre, extension = os.path.splitext(archivo_imagen)
        if extension != '.jp2':
            print("Error de formato de archivo.", end=" ")
            print("El archivo '" + nombre + extension + "' debe tener extensión .jp2")
            sys.exit()
    else:
        try:
            archivo = os.path.basename(archivo_imagen)
            portadora = Image.open(archivo_imagen)
        except FileNotFoundError:
            print("Error: no se encontró el archivo '" + archivo_imagen + "'")
            sys.exit()
        except OSError:
            print("Error: el archivo '" + archivo_imagen + "' no es una imagen.")
            sys.exit()
        formato = portadora.format
        if metodo == 'lsb' and formato != 'PNG':
            print("Error de formato de archivo.", end=" ")
            print("'" + archivo + "' debe ser PNG")
            sys.exit()
        if metodo == 'dct' and formato != 'JPEG' and formato != 'JPEG2000':
            print("Error de formato de archivo.", end=" ")
            print("'" + archivo + "' debe ser JPEG (.jpeg, .jpg) o JPEG2000 (.jp2)")
            sys.exit()
        elif metodo == 'dct_unhide' and formato != 'JPEG2000':
            print("Error de formato de archivo.", end=" ")
            print("'" + archivo + "' debe ser JPEG2000 (.jp2)")
            sys.exit()


def texto(archivo):
    '''Comprueba la extensión del archivo a ocultar.'''
    nombre, extension = os.path.splitext(archivo)
    if extension != '.txt':
        print("Error en extensión de archivo:", end=" ")
        print("'" + nombre + extension + "' debe ser .txt")
        sys.exit()


def clave_rsa(clave):
    '''Comprueba la extensión PEM de la clave RSA.'''
    nombre, extension = os.path.splitext(clave)
    if extension != '.pem':
        print("Error en extensión de archivo:", end=" ")
        print("'" + nombre + extension + "' debe ser .pem")
        sys.exit()


def clave_aes(clave):
    '''Comprueba la extensión .key de la clave RSA.'''
    nombre, extension = os.path.splitext(clave)
    if extension != '.key':
        print("Error en extensión de archivo:", end=" ")
        print("'" + nombre + extension + "' debe ser .key")
        sys.exit()
