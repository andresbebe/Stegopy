'''Módulo que implementa compresión y descompresión'''
import sys
import lzma

def compr_lzma(archivo):
    '''Comprime en formato LZMA.'''
    # lee el archivo y lo pasa a binario
    try:
        with open(archivo, 'r') as fichero:
            archivo_secreto = fichero.read()
        archivo_binario = archivo_secreto.encode('UTF-8')
    except FileNotFoundError:
        print("Error. No se encontró el archivo '" + archivo +"'")
        sys.exit()

    # comprime el binario en lzma
    binario_lzma = lzma.compress(archivo_binario)

    return binario_lzma

def descompr_lzma(mensaje_lzma, archivo):
    '''Descomprime en formato LZMA.'''
    # descomprime el mensaje
    texto_binario = lzma.decompress(mensaje_lzma)

    # binario a texto
    texto = texto_binario.decode("UTF-8")

    # almacena el mensaje en archivo
    with open(archivo, 'w') as variable:
        variable.write(texto)
