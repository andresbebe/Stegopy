STEGOPY
==

> ADVERTENCIA: esta herramienta aún se encuentra en fase de desarrollo, el uso de la misma queda bajo su exclusiva responsabilidad


__Stegopy__ es una herramienta de esteganografía escrita en __Python 3__. Implementa técnicas del dominio espacial (__LSB__, _último bit significativo_)  y del dominio de frecuencias (__DCT__, _transformada directa del coseno_).

En el caso de __LSB__ oculta un mensaje _almacenado en un archivo de texto_ dentro de una imagen en formato _png_. En el caso de __DCT__, se oculta una frase secreta _ingresada desde consola_ en una imagen de formato _jpeg_ o _jpeg2000_.

A grandes rasgos, __LSB__ es una técnica _poco resistente al estegoanálisis_ pero con _buena capacidad de ocultación_. Por ejemplo, una imagen de unos 2MiB de tamaño en formato _png_ puede ocultar un archivo de varias decenas de KiB. En contraste, __DCT__ es una técnica _bastante resistente al estegoanálisis_ pero posee una _capacidad de ocultación bastante pobre_. Si tomamos la misma imagen anterior de ejemplo, solamente podrá ocultar 3 o 4 centenas de caracteres.
  

#### Esteganografía por el método del último bit significativo (LSB) ####
  
El mensaje se oculta _comprimido_ y _cifrado_ mediante los algoritmos __LZMA__ y __AES__ respectivamente, con una _clave_ de _128 bits_, _padding_ mediante _PKCS7_  y _autenticación_ por
_SHA256_. Esta _clave AES_ se cifra con una _clave RSA_ de _4096 bits._ 

Los algoritmos de cifrado utilizan el módulo __cryptography__ (https://cryptography.io). Se usa cifrado simétrico AES para el mensaje ya que _con el cifrado asimétrico RSA no se pueden cifrar textos de mayor tamaño que la clave._

Otros módulos de terceros usados son:
* pillow (https://pypi.org/project/Pillow/)
* docopt (https://pypi.org/project/docopt/)
* opencv-python (https://pypi.org/project/opencv-python/)
* bitarray (https://pypi.org/project/bitarray/)
* numpy (https://pypi.org/project/numpy/)
* scikit-image (https://pypi.org/project/scikit-image/)
* scipy (https://pypi.org/project/scipy/)

El mecanismo de uso es el siguiente:
1. __stegopy__ genera una __clave AES__
2. luego _cifra_ al __mensaje__ con esta __clave AES__
3. posteriormente __cifra__ la __clave AES__ con la __clave pública RSA__ del destinatario.

El _emisor_ debe asegurarse de que el _destinatario_ obtenga la _clave AES cifrada con RSA_ para descifrarla con su _clave privada_ y utilizarla para poder _descifrar el mensaje_.

### Esteganografía por transformada directa del coseno (DCT) ###

Es una implementación del algoritmo esteganográfico por bloques de Zhao-Koch.
__Estado actual de la implementación:__ soporta la ocultación de mensajes ingresados por consola en imágenes en formato _jpeg_ o _jpeg2000_.

> **NOTA**: en la fase actual del desarrollo, la estego imagen de ocultación sólo se graba en formato JPEG2000 para evitar posibles errores en la revelación del mensaje oculto que podrían ser inducidos por la capa extra de compresión aportada por el formato jpeg.

### Forma de uso de stegopy (cli) ###

Se deben dar permisos de ejecución al script mediante el comando:
   ~~~
   sudo chmod +x stegopy
   ~~~
Para instalar los módulos necesarios, ejecutar:
   ~~~
   pip install -r requeriments.txt
   ~~~
Para cifrar un mensaje (según el método deseado):
   ~~~
   python stegopy dct hide [input] [output]
   python stegopy lsb hide [file] [clave_pub] [input] [output]
   ~~~
en donde: 

* [file] _es el archivo que contiene el texto a ocultar_
* [clave_pub]  _es la clave pública RSA (en formato PEM)_
* [input] _es la imagen portadora_ 
* [output] _es la imagen que contendrá al estego secreto_ 



### Ejemplos de uso ###

##### Ocultar con DCT #####
~~~
python stegopy dct hide [imagen-in.jpg] [imagen-out.jp2]
~~~
Usa la [imagen-in.jpg] para ocultar un secreto que se ingresa por consola y lo graba en el archivo [imagen-out.jp2]

##### Ocultar con LSB y cifrar #####
~~~
python stegopy lsb hide [archivo.txt] [RSA-publica.pem] [imagen-in.png] [imagen-out.png]
~~~    
Oculta el contenido del [archivo.txt] dentro de [imagen-in.png] para grabarlo en [imagen-out.png]. El contenido es comprimido junto a la clave AES y cifrado con la clave [RSA-publica.pem] 

##### Extraer con DCT #####
~~~
python stegopy dct unhide [input]
~~~
Revela el mensaje oculto en la imagen [input]

##### Extraer con LSB y descifrar #####
~~~
python stegopy lsb unhide [clave_priv] [clave_AES] [input] [output]
~~~
revela el archivo oculto en la imagen [input] y lo graba en el archivo [output] usando la clave RSA privada [clave_priv] y la [clave_AES]

##### Generacion de par de llaves #####
~~~
python stegopy test
~~~
genera una _pareja de claves RSA pública y privada en formato .pem_ y las almacena en el directorio raíz de stegopy. Estas claves se pueden usar para pruebas.

~~~
##### Ayuda y Version #####

python stegopy [opcion]
~~~
en donde [opcion] puede ser:
 * -h --help		_Muestra la ayuda_
 * --version		_Muestra la versión_

### Forma de uso de stegopy (web) ###



Actualizamos apt update , apt upgrade.

-Instalamos pip3,el paquete dev python3.6, y el paquete para generar el enviroment:
~~~
sudo apt-get install python3-pip python3.6-dev python3-venv python3-pip
~~~
Creamos el entorno virtual en:

/home/user/ambiente_virtual/ambiente  
~~~
virtualenv ambiente -p python3
~~~
-Activamos el entorno virtual:

desde /ambiente_virtual
~~~
source ambiente/bin/activate
~~~
Bajamos el Stegopy Web en la ruta donde activamos el entorno virtual:

/home/user/ambiente_virtual/ambiente
~~~
git clone https://gitlab.com/andresbebe/Stegopy
~~~
instalamos las librerias correspondientes con 
~~~
pip3 install -r requirements-cli-web.txt
~~~  
NOTA:Las librerias que se encuentran en requirements son las librerias que necesita la version web como la cli. En caso de haber usado la cli primero , se instalaran las web de lo contrario se instalaran completas.


Una vez que terminamos de instalar todo ,specificamos la ruta absoluta del espacio de trabajo del programa en la aplicacion:

Desde la consola editamos el archivo 
~~~
~/ambiente_virtual/ambiente/Stegopy/Stegopy Web/App.py  
~~~
Modificamos la Ruta de guardado:
~~~
a /home/usuario/ambiente_virtual/ambiente/Stegopy/Stegopy Web/
~~~
en el usuario ,su usuario en caso de lo corra en la carpeta home.

Para ejecutar el programa:

dentro de ~/ambiente_virtual/ambiente/Stegopy/Stegopy Web/
~~~
Ejecutar python3 App.py
~~~    
Para acceder al menu:
~~~
http://localhost:3000
~~~


##### Ocultar con DCT #####

Para ocultar el texto con DCT se debe ingresar la imagen y el texto:
~~~
1.Imagen (Archivo de formato .JPEG)    
2.Texto (Archivo de formato .TXT)
~~~
##### Ocultar con LSB y cifrar #####

Para ocultar una imagen con LSB es necesario primero generar las llaves (Llave publica y Llave Privada. Una vez generadas , se ingresa al menu Imagen y al item "Ocultar texto con LSB" y ahi ingresamos los 3 archivos:
~~~
1.Llave Publica (Archivo de formato .PEM)    
2.Imagen (Archivo de formato .PNG)    
3.Texto (Archivo de formato .TXT)
~~~
##### Extraer con DCT #####

Para extraer el texto con DCT se debe ingresar la imagen:
~~~
Imagen (Archivo de formato .JP2)  
~~~
##### Extraer con LSB y descifrar #####

Para extraer una imagen con LSB se debe ingresar la llave privada que se utilizo con la cual se oculto la imagen la Clave RSA y la imagen:
~~~
1.Llave Privada (Archivo de formato .PEM)   
2.Clave RSA (Archivo de formato .KEY)   
3.Imagen (Archivo de formato .PNG)
~~~


### TO DO ###

* Implementar LSB __HECHO__
* Implementar compresión LZMA (en LSB) __HECHO__
* Implementar DCT __HECHO__
* Implementar GUI __EN DESARROLLO__
* Implementar interfaz web __EN EVALUACION__
* Implementar otros algoritmos de cifrado __EN EVALUACION__
* Implementar otros formatos de archivo __EN EVALUACION__
* Implementar cifrado en el método DCT __EN DESARROLLO__

### Cómo contribuir ###

1) Escribiendo tus propios parches:

     1) Haz un fork de la rama `master` del repositorio
     2) Escribe tus parches en tu fork
     3) Haz un pull request o envía un email a `tatuss@ciudad.com.ar`
     4) De estar todo bien, agregaremos tus parches en la rama `develop` y, más
     tarde, en la rama `master`.
     
 2) Reportando bugs y/o solicitando nuevas características y/o funcionalidades
 
     1) POR FAVOR, te pedimos encarecidamente que completes la planilla
     predeterminada correspondiente (ver sección __ISSUES__) para simplificar la
     tarea de los desarrolladores.
     2) Si lo expuesto más arriba te resulta complicado o confuso, envía un
     email a `tatuss@ciudad.com.ar`

### Licencias ###

El software es licenciado bajo los términos de la Licencia Pública General de GNU versión 3 (GPLv3)
